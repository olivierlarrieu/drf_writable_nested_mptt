from copy import deepcopy
from mptt.exceptions import InvalidMove
from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework.serializers import model_meta


class MpttWritableNestedModelSerializer(WritableNestedModelSerializer):
    # pylint: disable=no-self-use
    def save_mptt_instance(self, parent_instance, new_instance):
        # save and move to parent
        new_instance.is_valid(raise_exception=True)
        new_instance.save()
        try:
            new_instance.instance.move_to(parent_instance)
        except InvalidMove:
            pass

    def create(self, validated_data):
        # call original method without children
        validated_data_copy = deepcopy(validated_data)
        validated_data_copy.pop('children', [])
        instance = super().create(validated_data_copy)

        ModelClass = self.Meta.model
        info = model_meta.get_field_info(ModelClass)
        children = []
        for field_name, relation_info in info.relations.items():
            if relation_info.to_many and (field_name in validated_data) and field_name == "children":
                children = validated_data.pop(field_name)
        # Save many-to-many relationships after the instance is created.
        # pylint: disable=too-many-nested-blocks
        if children:
            for data in children:
                new_instance = self.update_or_create_child(data, instance)

        return instance

    def update(self, instance, validated_data):
        # call original method without children
        info = model_meta.get_field_info(instance)
        validated_data_copy = deepcopy(validated_data)
        validated_data_copy.pop('children', None)
        instance = super().update(instance, validated_data_copy)

        children = validated_data.pop('children', [])
        if children:
            for data in children:
                new_instance = self.update_or_create_child(data, instance)
        else:
            instance.children.all().update(parent=None)
        instance.save()
        return instance

    def update_or_create_child(self, data, parent):
        if data.get('id') is not None:
            queryset = parent._meta.model.objects.filter(pk=data['id'])
            if queryset.exists():
                # remove child which are not in value
                data_instance = queryset.first()
                # unparent data_instance for move_to later
                data_instance.parent = None
                data_instance.save()
                data_instance.children.all().update(parent=None)
                new_instance = self.__class__(data_instance, data=data, partial=self.partial)
            else:
                new_instance = self.__class__(data=data)
        else:
            new_instance = self.__class__(data=data)
        self.save_mptt_instance(parent, new_instance)
        return new_instance
