# Surcharche de WritableNestedModelSerializer avec support de django Mptt

    Permet le support des relations Mptt.
 

# exemple

```python
# models
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class CategoryEnviron(models.Model):

    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Category(MPTTModel):

    name = models.CharField(max_length=30)
    environment = models.ForeignKey(
        CategoryEnviron,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name="categories"
    )
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['name']


class Product(models.Model):

    name = models.CharField(max_length=30)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name="products")

    def __str__(self):
        return self.name


```
```python
# serializers
from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from drf_writable_nested_mptt import MpttWritableNestedModelSerializer
from product.models import Category, CategoryEnviron, Product


class CategoryEnvironSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(allow_null=True, required=False)

    class Meta:
        model = CategoryEnviron
        fields = ['id', 'name', ]


class CategorySerializer(MpttWritableNestedModelSerializer):

    id = serializers.IntegerField(allow_null=True, required=False)
    children = RecursiveField(many=True, allow_null=True, required=False)
    environment = CategoryEnvironSerializer(allow_null=True, required=False)

    class Meta:
        model = Category
        fields = ['id', 'name', 'children', 'environment', ]


class ProductSerializer(WritableNestedModelSerializer):

    category = CategorySerializer()

    class Meta:
        model = Product
        fields = ['id', 'name', 'category', ]

```