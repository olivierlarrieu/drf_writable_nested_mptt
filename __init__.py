__title__ = 'DRF writable nested Mptt'
__version__ = '0.0.1'
__author__ = 'Larrieu Olivier'
__license__ = 'BSD 2-Clause'
__copyright__ = ''

VERSION = __version__

from .serializers import MpttWritableNestedModelSerializer
