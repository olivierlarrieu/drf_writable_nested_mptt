from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class CategoryEnviron(models.Model):

    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Category(MPTTModel):

    name = models.CharField(max_length=30)
    environment = models.ForeignKey(
        CategoryEnviron,
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name="categories"
    )
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['name']


class Product(models.Model):

    name = models.CharField(max_length=30)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name="products")

    def __str__(self):
        return self.name
