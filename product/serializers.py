from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from drf_writable_nested_mptt import MpttWritableNestedModelSerializer
from product.models import Category, CategoryEnviron, Product


class CategoryEnvironSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(allow_null=True, required=False)

    class Meta:
        model = CategoryEnviron
        fields = ['id', 'name', ]


class CategorySerializer(MpttWritableNestedModelSerializer):

    id = serializers.IntegerField(allow_null=True, required=False)
    children = RecursiveField(many=True, allow_null=True, required=False)
    environment = CategoryEnvironSerializer(allow_null=True, required=False)

    class Meta:
        model = Category
        fields = ['id', 'name', 'children', 'environment', ]


class ProductSerializer(WritableNestedModelSerializer):

    id = serializers.IntegerField(allow_null=True, required=False)
    category = CategorySerializer()

    class Meta:
        model = Product
        fields = ['id', 'name', 'category', ]
