import pprint
from collections import OrderedDict
from rest_framework.test import APITestCase
from .models import Product, CategoryEnviron, Category
from .serializers import CategorySerializer, ProductSerializer


super_print = pprint.PrettyPrinter(indent=4).pprint


class Tests(APITestCase):

    def setUp(self):
        # un environment
        self.environ = CategoryEnviron.objects.create(name="develop")

        # 2 categories dont une enfant
        self.cat1 = Category.objects.create(name="cat 1", environment=self.environ)
        self.cat2 = Category.objects.create(name="cat 2", environment=self.environ)
        self.cat2.move_to(self.cat1)

        # un produit
        self.product = Product.objects.create(name="product 1", category=self.cat1)

    def test_create_product(self):
        data = OrderedDict({
            "id": 2000,
            "name": "SUPER PRODUCT",
            "category": {
                "id": 1,
                "name": "CATEGORY 1",
                "environment": {"name": "ENVIRONMENT 1"},
                "children": [
                    {
                        "id": self.cat2.pk,
                        "name": "CATEGORY 2 UPDATED",
                        "environment": {"name": "ENVIRONMENT 2"},
                        "children": [
                            {
                                "id": 3,
                                "name": "CATEGORY 3",
                                "children": [
                                    {
                                        "id": 4,
                                        "name": "CATEGORY 4",
                                        "children": [
                                            {
                                                "id": 5,
                                                "name": "CATEGORY 5",
                                                "environment": {"name": "ENVIRONMENT 3"},
                                                "children": []
                                            },
                                            {
                                                "id": 6,
                                                "name": "CATEGORY 6",
                                                "environment": {"name": "ENVIRONMENT 4"},
                                                "children": []
                                            },
                                            {
                                                "id": 7,
                                                "name": "CATEGORY 7",
                                                "environment": {"id": self.environ.pk, "name": "develop"},
                                                "children": []
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "id": 8,
                        "name": "cat test",
                        "children": []
                    }
                ]
            }
        })
        serializer = ProductSerializer(data=data)
        serializer.is_valid()
        serializer.save()

        # il doit y avoir 8 categories, 7 créees et une mise à jour (pk=2)
        self.assertEqual(Category.objects.count(), 8)

        # il ne doit y avoir que 2 produit
        self.assertEqual(Product.objects.count(), 2)

        # il ne doit y avoir que 5 environments
        self.assertEqual(CategoryEnviron.objects.count(), 5)
        # category 7 a l environment pk self.environ.pk
        self.assertEqual(self.environ.pk, Category.objects.get(pk=7).environment.pk)
        # la category pk=2 est mise a jours
        self.assertEqual(Category.objects.get(pk=self.cat2.pk).name, "CATEGORY 2 UPDATED")

    def test_update_product(self):
        self.test_create_product()
        data = OrderedDict({
            "name": "SUPER PRODUCT UPDATED",
            "category": {
                "id": self.cat2.pk,
                "name": "cat 2 updated",
                "environment": {"id": self.cat2.environment.pk, "name": "ENVIRONMENT 2 UPDATED"},
                "children": []
            },
        })
        serializer = ProductSerializer(Product.objects.get(pk=2000), data=data)
        serializer.is_valid()
        serializer.save()
        
        # le produit a ete mis a jours
        self.assertEqual(Product.objects.get(pk=2000).name, "SUPER PRODUCT UPDATED")

        # la category 2 n'a plus d enfants et son nom est mis a jours
        self.assertEqual(Category.objects.get(pk=self.cat2.pk).children.count(), 0)
        self.assertEqual(Category.objects.get(pk=self.cat2.pk).name, "cat 2 updated")
        self.assertEqual(Category.objects.get(pk=self.cat2.pk).environment.name, "ENVIRONMENT 2 UPDATED")
        # il y a toujours 8 categories
        self.assertEqual(Category.objects.count(), 8)
